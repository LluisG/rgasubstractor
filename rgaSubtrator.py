
import sys
from PyQt5 import QtCore, QtWidgets, QtGui
from PyQt5.QtWidgets import QMainWindow, QLabel, QGridLayout, QWidget, \
    QHBoxLayout, QVBoxLayout, QMessageBox
from PyQt5.QtWidgets import QPushButton
from PyQt5.QtCore import QSize, QDir
from PyQt5 import QtWidgets
from pyqtgraph import PlotWidget, plot, FileDialog
import pyqtgraph as pg
import sys  # We need sys so that we can pass argv to QApplication
import os
from PyQt5 import QtWidgets
from pyqtgraph import PlotWidget, plot
import pyqtgraph as pg
import sys  # We need sys so that we can pass argv to QApplication


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        widget = QWidget()
        self.setWindowTitle("RgaSubstrator")

        pybutton = QPushButton('Select File', self)
        pybutton.clicked.connect(self.clickMethod)
        pybutton.resize(100, 32)
        self.textLine = QLabel("File")
        self.textLine.setStyleSheet(" border: 1px solid black")


        self.slider = QtWidgets.QSlider
        self.vertical_layout = QVBoxLayout()
        self.vertical_layout.addWidget(pybutton)
        self.vertical_layout.addWidget(self.slider(1))
        # self.vertical_layout.addWidget(self.text)
        self.vertical_layout.addWidget(self.textLine)

        self.main_layout = QHBoxLayout()


        self.graphLayout = pg.LayoutWidget()


        self.graph = pg.PlotDataItem()
        self.graph.setLogMode(False,True)
        self.graphLayout.addWidget(self.graph)

        # self.pw1.addItem(self.graph)
        # self.pw1 = pg.PlotWidget()
        # self.setCentralWidget(self.graphWidget)


        self.setMinimumSize(QSize(300, 200))
        self.setWindowTitle("RGA Substractor")


        # pybutton.move(50, 50)

        # self.main_layout.addWidget(self.graphWidget)


        # self.main_layout.addWidget(self.graph)

        self.main_layout.addLayout(self.graphLayout)

        self.main_layout.addLayout(self.vertical_layout)
        self.main_layout.minimumHeightForWidth(0.5)

        widget.setLayout(self.main_layout)
        self.setCentralWidget(widget)

        self.massList=[]
        self.pressuresList=[]
        self.n_scans=0

        self. hour = [1,2,3,4,5,6,7,8,9,10]
        self.temperature = [30,32,34,32,33,31,29,32,35,45]
    def clickMethod(self):
        fileName, _ = FileDialog.getOpenFileName(self, 'txt rga files',
                                                     QDir.homePath(),
                                                     '*.txt')

        if fileName:
            file1 = open(fileName, 'r')
            lines=file1.readlines()
            if not lines[0] == ('"Spectra International Data File"\n'):
                print(lines[0])
                QMessageBox.warning(self, "Not a MKS RGA file",
                                    "Ensure to choose a MKS RGA file")

            else:
                self.textLine.setText(os.path.split(fileName)[-1])
                for line in lines:

                    if line.startswith('"Time"'):
                        self.take_masses(line)

                for line in lines [59:]:
                        self.take_pressures(line)



        # print('Clicked Pyqt button.')
        print(len(self.massList))
        print(len(self.pressuresList[0]))
        print(self.massList)
        print(self.pressuresList[0])
        self.graph.plot(self.massList, self.pressuresList[0])

    def take_masses(self, line):
        """take masses list in float format"""
        massLine = line.replace('"Mass ', "")
        massLine = massLine.replace('"', '')
        massLine = massLine.split()
        del massLine[0:2]
        del massLine[-3:]
        massLine= [float(x) for x in massLine]
        print(massLine)
        self.massList=massLine
        # return massLine

    def take_pressures(self, line):
        ppLine = line.replace('"', '')
        ppLine = ppLine.split()
        ppLine = ppLine[4:-1]

        ppLine = [float(x) for x in ppLine]
        self.pressuresList.append(ppLine)

        self.n_scans +=1
        # print(self.ppressures)
        # print(self.n_scans)




if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    mainWin = MainWindow()
    mainWin.show()
    sys.exit( app.exec_() )




