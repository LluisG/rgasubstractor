import sys

from PyQt5 import QtGui
from PyQt5.QtWidgets import QApplication, QLabel, QMainWindow
from PyQt5.QtCore import Qt
from BackGroundSubtractor import backGroundSubs
# Subclass QMainWindow to customise your application's main window

class MainWindow(QMainWindow):

    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)

        self.setWindowTitle("RGA Subtractor")
        self.mainSub=backGroundSubs()

        self.menubar = self.menuBar()
        help = self.menubar.addMenu("Help")

        self.setCentralWidget(self.mainSub)





    def set_hasFile(self):
        self.mainSub.set_hasFile()

    def closeEvent(self, a0: QtGui.QCloseEvent) -> None:
        self.set_hasFile()


app = QApplication(sys.argv)

window = MainWindow()
window.show()
ret = app.exec_()
sys.exit(ret)
# app.exec_()
