import threading

from PyQt5.QtWidgets import QWidget, QDialog, QApplication
from PyQt5 import QtWidgets
from pyqtgraph import PlotWidget

from rgaSubtractor import PlotScan
from PyQt5.QtWidgets import QMainWindow, QLabel, QGridLayout, QWidget, \
    QHBoxLayout, QVBoxLayout, QMessageBox
import os
import sys
import threading as th


class backGroundSubs(QtWidgets.QMainWindow):
    def __init__(self, *args, **kwargs):
        super(backGroundSubs, self).__init__(*args, **kwargs)
        widget = QWidget()

        #
        # def __init__(self, *args, **kwargs):
        #     super(MainWindow, self).__init__(*args, **kwargs)
        #     widget = QWidget()

        # super().__init__()

        self.assembly = PlotScan()
        self.background = PlotScan()

        self.layout = QVBoxLayout()

        self.plotSubstraction = PlotWidget()

        self.layout.addWidget(self.assembly)
        self.layout.addWidget(self.background)
        self.layout.addWidget(self.plotSubstraction)
        # widget.setLayout(self.layout)
        # self.setCentralWidget(widget)
        # self.setWindowTitle("RGA Substractor")

        # self.setLayout(self.layout)
        self.setWindowTitle("RGA subtract v2")

        # self.masses = self.assembly.get_masses()
        # self.set_pressures()

        self.pressures1 = []
        self.pressures2 = []

        self.hasFiles = threading.Event()
        self.pressureTaker = th.Thread(target=self.set_pressures)

        self.pressureTaker.start()

        self.pressureToCompare1 = []
        self.pressureToCompare2 = []

        widget.setLayout(self.layout)

        self.setCentralWidget(widget)

    def set_pressures(self):
        while not self.hasFiles.is_set():

            print(self.assembly.hasFile, self.background.hasFile)

            if self.assembly.hasFile and self.background.hasFile:
                self.pressureAssembly = self.assembly.get_pessures()
                self.pressureBackground = self.background.get_pessures()

                # self.hasFiles.set()
                self.set_hasFile()
                self.plotUpdater = th.Thread(target=self.updatePressure())
                self.plotUpdater.start()

    def set_hasFile(self):
        self.hasFiles.set()

    def updatePressure(self):
        # if self.assembly.hasFile and self.background.hasFile:

        print("WWWWWWWWWWW")

        if self.pressureToCompare1 != self.assembly.get_pessures():
            self.do_subtraction()
        else:
            self.pressureToCompare1 = self.assembly.get_pessures()
            # b = self.background.get_pessures()

        if self.pressureToCompare2 != self.background.get_pessures():
            self.do_subtraction()
        else:
            self.pressureToCompare2 = self.background.get_pessures()

        # if a == self.pressureBackground:
        #     self.do_subtraction()
        # else:
        #     self.pressureBackground = b

    # def do_subtraction(self):
    #     print(len(self.assembly.get_pessures()))
    #     print(len(self.background.get_pessures()))
    #     if len(self.assembly.get_pessures()) != len(self.background.get_pessures()):
    #          QMessageBox.warning(self, "Different file size",
    #                                 "The files has not the same quantity of points")
    #     else:
    #         print("HOLE")
    #         self.plot.clear()
    #         subtraction = self.assembly.get_pessures() - self.background.get_pessures()
    #         self.plot.plot(self.masses, subtraction)

    def do_subtraction(self):
        # print(self.assembly.get_masses())
        # print(self.background.get_masses())
        global massList
        pressures_1 = self.assembly.get_pessures()
        pressures_2 = self.background.get_pessures()
        print("111=", len(pressures_1))
        print("222=", len(pressures_2))
        print("11=", pressures_1)
        print("22=", pressures_2)
        # massList1 = self.assembly.get_masses()
        # massList2 = self.background.get_masses()
        len1 = len(self.assembly.get_masses())
        len2 = len(self.background.get_masses())

        # self.pressures1= self.assembly.get_pessures()
        # self.pressures2= self.background.get_pessures()

        # print("M1_1=", massList1)
        factor = len1 / len2
        print("FACTOR=", factor)
        if factor > 1:
            print("1_1=", len(pressures_1))
            pressures_1 = (pressures_1[0:-1:int(factor)])
            print("1_1=", len(pressures_1))
            massList = self.background.get_masses()
            # pressures_1=self.background.get_pessures()

            # print(len(massList1))
            # massList1=(massList1[0:-1:int(factor)])
            # print("M1=", massList1)
            # print("M2=",massList2)
        else:

            factor2 = 1 / factor
            pressures_2 = (self.pressures2[0:-1:int(factor2)])
            print("2_2=", len(self.pressures2))
            # massList = self.background.get_masses()
            # massList2=(massList2[1:-1:int(factor2)])

        # scan1 = list (zip(self.assembly.get_masses, self.assembly.get_pessures) )
        # scan2 = list (zip(self.background.get_masses, self.background.get_pessures))
        # print(scan1)
        # print(scan2)
        print("1=", len(pressures_1))
        print("2=", len(pressures_2))
        print("11=", pressures_1)
        print("22=", pressures_2)

        # print("1=",len(massList1))
        # print("2=", len(massList2))
        # print("11=",massList1)
        # print("22=",massList2)

        # if len(self.assembly.get_pessures()) != len(self.background.get_pessures()):
        #      QMessageBox.warning(self, "Different file size",
        #                             "The files has not the same quantity of points")
        # else:
        print("HOLE")
        # self.plotSubstraction.clear()

        #
        subtraction = []
        for i in range(0, len(pressures_1)):
            subs = pressures_1[i] - pressures_2[i]
            if subs < 1e-15:
                subs = 1e-15
            subtraction.append(subs)

        print(len(subtraction))
        print(subtraction)

        # subtraction = self.pressures1-self.pressures2

        h2o = massList.index(18.0)
        print(h2o)
        self.plotSubstraction.plot(massList, subtraction)


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    mainWin = backGroundSubs()
    mainWin.show()
    sys.exit(app.exec_())



    # app = QApplication(sys.argv)
    # dialog = MainWindow()
    # sys.exit(dialog.exec_())
